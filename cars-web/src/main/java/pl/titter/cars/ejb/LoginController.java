/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.titter.cars.ejb;

import java.io.Serializable;

import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.servlet.http.HttpSession;
import org.modelmapper.ModelMapper;
import pl.titter.cars.ejb.model.Client;
import pl.titter.cars.ejb.model.User;


/**
 *
 * @author Dominik
 */
@ManagedBean(name = "LoginCtrl")
@SessionScoped
public class LoginController implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @EJB
    private ClientBeanIfc clientBean;

    private String userLogin;
    private Boolean logged = false;
    
    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
        
    }

    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }
    
    public void login(){
        System.out.println("ZALOGOWANO!");
        this.logged = true;
        User u = new User();
        u.setLogin(this.userLogin);
        u = clientBean.create(new Client());
        ModelMapper mapper = new ModelMapper();
        this.client = mapper.map(u, UserDto.class);
        this.user.setCityZip(u.getZipCode()+"/"+u.getCity());
    }
    
    @PreDestroy
    public void logout(){
        System.out.println("WYLOGOWANO");
        this.logged = Boolean.FALSE;
        ((HttpSession) FacesContext.
                getCurrentInstance().
                getExternalContext().
                getSession(false)).invalidate();
    }
}
