/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.titter.cars.ejb;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 *
 * @author Dominik
 */
@ManagedBean(name = "RegClientCtrl")
@SessionScoped
public class RegClientController implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @EJB
    private ClientBeanIfc clientBean;
    
    /**
     * Creates a new instance of LoginController
     */
    public RegClientController() {
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientBeanIfc getClientBean() {
        return clientBean;
    }

    public void setClientBean(ClientBeanIfc clientBean) {
        this.clientBean = clientBean;
    }

    
}
