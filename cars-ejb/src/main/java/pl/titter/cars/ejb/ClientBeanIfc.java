/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.titter.cars.ejb;


import javax.ejb.Local;
import javax.ejb.Remote;
import pl.titter.cars.ejb.model.Address;
import pl.titter.cars.ejb.model.Car;
import pl.titter.cars.ejb.model.Client;


/**
 *
 * @author Adrian
 */
@Local
@Remote
public interface ClientBeanIfc {    

    public Client create(Client c);
    public Client findByName(String fullName);
    public void remove(Client c);
    public void changeAddress(Address address);
    public void addCar(Car c);
}
