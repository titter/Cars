package pl.titter.cars.ejb.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Dominik
 */
@Entity
@DiscriminatorValue(value = "C")
public class Client extends User implements Serializable {

    @Column
    private String pesel;
    
    @Column
    private String nip;

    @OneToMany(mappedBy = "client")
    private List<Car> cars;

    public String getPesel() {
        return this.pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }


    public List<Car> getCars() {
        return this.cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public int getAge(){
        int age = Integer.valueOf(this.pesel.substring(2, 3));
        String century = "19";
        
        switch (age){
            case 0:
            case 1: century = "19";
            break;
            case 2:
            case 3: century = "20";
            break;
            case 4:
            case 5: century = "21";
            break;
            case 6:
            case 7: century = "22";
            break;
            case 8:
            case 9: century = "18";
        }
    
        int year = Integer.valueOf(century + this.pesel.substring(0, 2));
        return LocalDate.now().getYear() - year;
    }

    /**
     * @return the nip
     */
    public String getNip() {
        return nip;
    }

    /**
     * @param nip the nip to set
     */
    public void setNip(String nip) {
        this.nip = nip;
    }
}