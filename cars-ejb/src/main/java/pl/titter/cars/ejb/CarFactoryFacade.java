/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.titter.cars.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import pl.titter.cars.ejb.model.CarFactory;

/**
 *
 * @author Dominik
 */
@Stateless
public class CarFactoryFacade extends AbstractFacade<CarFactory> {

    @PersistenceContext(unitName = "carsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CarFactoryFacade() {
        super(CarFactory.class);
    }
    
}
