package pl.titter.cars.ejb.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Dominik
 */
@Entity
@Table(name = "cars")
@NamedQueries(value = {
    @NamedQuery(name = "findAllByEngine", query = "select c from Car c, Engine e where e = :enginename"),
    @NamedQuery(name = "findAllByYear", query = "select c from Car c where c.prodYear = :year"),
    @NamedQuery(name = "findAllByFactory", query = "select c from Car c, CarFactory cf where cf.name = :factoryname"),
    @NamedQuery(name = "findAllByModel", query = "select c from Car c, CarModel cm where cm.name = :modelname")
})
public class Car implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "reg_doc_no")
    private String regDocNo;
    
    @Column(name = "oc_no")
    private String ocNo;
    
    @Column
    private Integer mileage;
    
    @Column
    private String vin;

    @Column(name = "prod_year")
    private Integer prodYear;
    
    @Column
    private String description;
    
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "client_id")
    private Client client;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "engine_id")
    private Engine engine;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "factory_id")
    private CarFactory carFactory;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "model_id")
    private CarModel carModel;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "type_id")
    private CarType carType;
    
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "fuel_id")
    private FuelType fuelType;
    
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "gear_id")
    private GearType gearType;
    

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return this.vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getProdYear() {
        return this.prodYear;
    }

    public void setProdYear(Integer prodYear) {
        this.prodYear = prodYear;
    }

    public Engine getEngine() {
        return this.engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public CarFactory getCarFactory() {
        return this.carFactory;
    }

    public void setCarFactory(CarFactory carFactory) {
        this.carFactory = carFactory;
    }

    public CarModel getCarModel() {
        return this.carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    /**
     * @return the regDocNo
     */
    public String getRegDocNo() {
        return regDocNo;
    }

    /**
     * @param regDocNo the regDocNo to set
     */
    public void setRegDocNo(String regDocNo) {
        this.regDocNo = regDocNo;
    }

    /**
     * @return the ocNo
     */
    public String getOcNo() {
        return ocNo;
    }

    /**
     * @param ocNo the ocNo to set
     */
    public void setOcNo(String ocNo) {
        this.ocNo = ocNo;
    }

    /**
     * @return the mileage
     */
    public Integer getMileage() {
        return mileage;
    }

    /**
     * @param mileage the mileage to set
     */
    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    /**
     * @return the carType
     */
    public CarType getCarType() {
        return carType;
    }

    /**
     * @param carType the carType to set
     */
    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    /**
     * @return the fuelType
     */
    public FuelType getFuelType() {
        return fuelType;
    }

    /**
     * @param fuelType the fuelType to set
     */
    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    /**
     * @return the gearType
     */
    public GearType getGearType() {
        return gearType;
    }

    /**
     * @param gearType the gearType to set
     */
    public void setGearType(GearType gearType) {
        this.gearType = gearType;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

}