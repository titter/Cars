/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.titter.cars.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import pl.titter.cars.ejb.model.Address;
import pl.titter.cars.ejb.model.Car;
import pl.titter.cars.ejb.model.Client;

/**
 *
 * @author martin
 */
@Stateless
public class ClientBean implements ClientBeanIfc{
    
    @PersistenceContext(unitName = "carsPU")
    private EntityManager em;

    public ClientBean() {

    }
    
    @Override
    public Client create(Client c){
        try{
           Query q = em.createQuery("from Client c");
           List<Client> users = q.getResultList();
           boolean alreadyIn = false;
           for(Client tmp_user: users){
               if (c!=null && tmp_user!=null && tmp_user.getFirstName().equals(c.getFirstName())){
                   alreadyIn = true;
                   c = tmp_user;
                   break;
               }
           }
           
           if (!alreadyIn) em.persist(c);
           return c;
        }catch(Exception e){
            
        }
        return c;
    }

    @Override
    public Client findByName(String fullName) {
        Client clientToReturn = null;
        try {
            String[] fullNameTab = fullName.split(" ");
            Query q = em.createNamedQuery("findByName");
            q.setParameter("firstname", fullNameTab[0]);
            q.setParameter("lastname", fullNameTab[1]);
            
            List<Client> clients = q.getResultList();
            if (clients.size()>1){
                throw new Exception("More then one customer named: " + fullName + " was found in DB.");
            }else{
                if (clients.isEmpty()){
                    throw new Exception("There is noone in DB named: " + fullName);
                }else{
                    clientToReturn = clients.get(0);
                }
            }
        } catch (Exception e) {
            System.out.println("FINDBYNAME ERROR: " + e.getMessage());
        }
        return clientToReturn;
    }

    @Override
    public void remove(Client c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeAddress(Address address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addCar(Car c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
